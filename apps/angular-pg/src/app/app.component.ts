import { Component } from '@angular/core';

@Component({
  selector: 'sgnl-monorepo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angular-pg';
}
