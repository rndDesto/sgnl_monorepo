

## 0.0.9 (2023-09-25)


### Bug Fixes

* tambah text ([b7f90fa](https://gitlab.com/rndDesto/sgnl_monorepo/commit/b7f90faa2ca813c97054d883393c276463ed8895))

## 0.0.8 (2023-09-25)


### Bug Fixes

* home cmp ([826185c](https://gitlab.com/rndDesto/sgnl_monorepo/commit/826185c01d93cc41c2f2553a8b7c6a5fd8e2dfac))
* home componen ([211181a](https://gitlab.com/rndDesto/sgnl_monorepo/commit/211181a4c4c424db54abb9d07017fd9ea745034c))

## 0.0.7 (2023-09-25)


### Bug Fixes

* script build paket ([677c340](https://gitlab.com/rndDesto/sgnl_monorepo/commit/677c3405f7b31e72c5896b3af6e89f718fbfd351))

## 0.0.6 (2023-09-25)


### Bug Fixes

* revisi componen angular ([5b15528](https://gitlab.com/rndDesto/sgnl_monorepo/commit/5b15528ab0f3f097e14ffaf449a1308cbab826d9))

## 0.0.5 (2023-09-25)


### Bug Fixes

* tambah npm rc ([d782c16](https://gitlab.com/rndDesto/sgnl_monorepo/commit/d782c167208f07acabee673977ea12738e56b996))

## 0.0.4 (2023-09-25)


### Bug Fixes

* angular library ([c1227ba](https://gitlab.com/rndDesto/sgnl_monorepo/commit/c1227ba23c56d37c7c49e731148e198b5469eeb9))

## 0.0.3 (2023-09-25)


### Bug Fixes

* playgorund react ([31c5d71](https://gitlab.com/rndDesto/sgnl_monorepo/commit/31c5d71a6ef9e851f3fc6f77b4b9553260480bf6))

## 0.0.2 (2023-09-25)


### Bug Fixes

* angular spancs ([6fa47dd](https://gitlab.com/rndDesto/sgnl_monorepo/commit/6fa47dde2f8ba5fd49c1f700e61edd6daf963c22))
* ini release it ([8c9e1be](https://gitlab.com/rndDesto/sgnl_monorepo/commit/8c9e1be13c55a38c4743270425db4dcfa297beb0))


### Chore

* release v0.0.1 ([22dc070](https://gitlab.com/rndDesto/sgnl_monorepo/commit/22dc07013ad597bc953ed664763b31a534fd1849))