import { Component, Input} from '@angular/core';

@Component({
  selector: 'sgnl-monorepo-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss'],
})

export class SignalHeadingComponent{
  @Input() title = '';
}
