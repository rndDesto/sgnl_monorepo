import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalHeadingComponent } from './heading.component';

describe('SignalHeadingComponent', () => {
  let component: SignalHeadingComponent;
  let fixture: ComponentFixture<SignalHeadingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SignalHeadingComponent]
    });
    fixture = TestBed.createComponent(SignalHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
