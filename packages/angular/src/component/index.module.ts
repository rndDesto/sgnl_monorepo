import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignalHeadingComponent } from './heading/heading.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    SignalHeadingComponent
  ],
})
export class AngularTypographyModule {}
