import { Component } from '@angular/core';

@Component({
  selector: 'sgnl-monorepo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {}
